$(document).ready( function() {
  // Hide the breadcrumb details, if no breadcrumb.
  $('#edit-brainstorm-breadcrumb').change(
    function() {
      div = $('#div-brainstorm-breadcrumb-collapse');
      if ($('#edit-brainstorm-breadcrumb').val() == 'no') {
        div.slideUp('slow');
      } else if (div.css('display') == 'none') {
        div.slideDown('slow');
      }
    }
  );
  if ($('#edit-brainstorm-breadcrumb').val() == 'no') {
    $('#div-brainstorm-breadcrumb-collapse').css('display', 'none');
  }
  $('#edit-brainstorm-breadcrumb-title').change(
    function() {
      checkbox = $('#edit-brainstorm-breadcrumb-trailing');
      if ($('#edit-brainstorm-breadcrumb-title').attr('checked')) {
        checkbox.attr('disabled', 'disabled');
      } else {
        checkbox.removeAttr('disabled');
      }
    }
  );
  $('#edit-brainstorm-breadcrumb-title').change();
} );
